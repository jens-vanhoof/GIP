+++
categories = ["Engels"]
date = "2015-12-05T14:46:35+02:00"
tags = ["Gip"]
title = "IT-autobiography"
+++

**My name is Jens Van Hoof, a 17-year old! student at the Immaculata institute in Oostmalle. Currently I'm studying IT-management. Most of the time, I'm the guy who helps people who don't get it yet. Later I want to be a programmer of software or games. My dream job is to be a hacker for the state.**

<img width="400px" height="300px" align="right" src="https://scontent-ams2-1.xx.fbcdn.net/hphotos-xta1/t31.0-8/12094781_914998871870460_7303528174420982861_o.jpg"></img>

I was about the age of 11 when my older brother went to college and he made a mini version of Pokémon. It made me interested in how he did it. So he told me I had to look at JavaScript. From that moment on my life changed. I began to search on the internet and I found some tutorials that gave me a basic insight. When I had learned the basics I wanted to learn more so I decided to go for IT management in secondary school.

My whole life is about IT. When I come home from school I turn on my pc and it stays on till I go to sleep. That’s about 8 hours a day, I know it’s pretty much but I think that IT is and will be the future. Most of the time when I’m on my pc I’m playing video games. But for school I also need my pc. Most of our homework is online like all our GIP assignments. I often think about making a network at home but I haven’t found the right time yet. I also play a lot of different games like League of Legends, Minecraft, Counterstrike, etc... I don’t often make a website because I’m not so good with designs but when I have to do it I can make a decent site. When I play some games or use a program I’m always fascinated with how they make something. Sometimes I even try to find out what language they used and try it out myself. I don’t think I’m a pro but I can say that I’m an advanced programmer in the early stage.

In the future I hope to learn more and more about different things in the IT world, not only programming but also some system management. My ambitions are to become a pretty good hacker but not for the illegal stuff. Maybe if I have the chance, become a hacker for the state.


I think there has been a big evolution in my programming skills. When I first began to program with C# I was happy I could make a program to print some text. Now I’m happy that I can make a whole game in C# or another language.
IT has become a really important part of my life. Like one day my keyboard didn’t work anymore and didn’t have a spare one. I couldn’t make my homework, so I had nothing to do. I don’t think I could live without my pc for a month.
