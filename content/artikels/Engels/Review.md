+++
categories = ["Engels"]
date = "2015-12-05T14:46:35+02:00"
tags = ["Gip"]
title = "Review"
+++

I’m going to review the VMware ESXi. The VMware ESXi is a program that can host multiple virtual servers on a physical server. The program makes the life of a system manager a lot easier. I learned about this software at my apprenticeship where they used it to host about  30 servers.


The VMware software is established by seven people Diane Greene, Scott Devine, Edward Wang, Mendel Rosenblum and Edouard Bugnion. The software was developed to create a safer and more controllable environment. Back in the days the server room was a room with about 30 physical servers, for each process another one, but that took a lot of space and it was hard to keep up the controls for the hardware. With VMware this problem is solved: now you run VMware on one physical server and then you can host multiple virtual servers.


VMware isn’t the only developer, there is also a program called Hyper-V from Microsoft but I never used it so maybe you can give that also a try. At my apprenticeship they started with VMware a long time ago because it was the best at that time. But if they had to choose now they would maybe take another developer.


Now you are asking yourself how the program itself works. You have one client with software running and from there you can control the multiple virtual servers on your physical server. One of  the software features is that you can easily manage your servers from one pc, so you don’t have to go to the server to change something.
In my opinion this is software pretty good, I’ll explain why. First the advantages. VMware is also a much better way than how they did it before virtualization.However there are also some disadvantages. With virtualization all your virtual servers are running on one physical server but when that goes down every server goes down too. But if you have two physical servers you can manage them with the software that the one takes over from the other one when one of the two goes down. Another disadvantage is that it isn’t the cheapest software on the market.

##Conclusion:7.5/10

My conclusion is that it is a very good program because it has a nice web interface where you can get a lot of statistics of the server about downtime and usage. I would recommend it to other companies especially for big companies because the limit for virtual servers is very high.
