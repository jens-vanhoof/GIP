+++
categories = ["Systeem en Netwerkbeheer"]
date = "2015-10-22T18:46:35+02:00"
tags = ["Gip"]
title = "Case Study"
+++

Tijdens mijn case study heb ik de software VMware ESXi.
Het word gebruikt alleen door de IT-Afdeling van de gemeente.
Hierdoor kan de gemeente meerdere virtuele servers hosten op een physieke server.
<img width="400px" height="300px" align="right" src="http://www.kernsafe.cn/images/Citrix-iSCSI-iStorage-Server.png"></img>

Ik vind dat de software wel goed werkte op mijn stagebedrijf. Er waren ook enkele voordelen hiervan zoals: ze konden makkelijk back-ups maken van alle servers, minder kosten enzovoort. Doordat de virtuele server kunnen bestuurd worden van een andere pc die met het netwerk verbonden is moesten ze niet altijd naar de serverruimte te gaan voor aanpassing.
