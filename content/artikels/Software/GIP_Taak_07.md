+++
categories = ["Software"]
date = "2015-10-22T18:46:35+02:00"
tags = ["Gip"]
title = "Workflow: Hugo vs WordPress"
+++

Hugo is een programma dat werkt met het implementeeren van codes terwijl Wordpress werkt met een drag and drop systeem. Daardoor is Wordpress veel makkelijker en beter om mee te starten. Hugo daar in tegen is een veel moeilijker programma want daar moet je alle code zelf gaan implenteren. Doordat de documentie niet zo denderend is is het ook zeer moeilijk om hier aan te beginnen. Nu we alle nadelen hebben besproken van Hugo zijn er ook een paar voordelen. Het sterke punt van hugo is dat je heel gamakkelijk je post kan maken en aanpassen.

---
---
---

# Een overzicht van de directory-structuur

#####  links

<a href="https://gitlab.com/jens-vanhoof/GIP">Gitlab</a> -
<a href="https://github.com/jens-vanhoof/GIP">Github</a> -
<a href="http://jens-vanhoof.github.io/GIP/  ">Public</a>

#####  Content   

- Hierin komen alle post die worden aangemaakt.

##### Layouts

  - index.html: Dit zorgt voor mijn home page en gebruikt.
  - partials : Hierin staan bijna alle elementen die ik gebruik in mijn site bv elke pagina die       ik maak gebruik de header.html en footer.html.
  - default : Deze map wordt gebruikt als er in partails geen gepaste file bevind dat hugo nodig heeft gebruikt het deze files.

##### Static

   - Hierin bevinden zich de css en javascript files van de site.   

##### Rest

   - Config.toml: Hierin kan je verschillende veriablen declareren die je met hugo kan aanroepen.
