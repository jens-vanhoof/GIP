+++
categories = ["IT-tapa"]
date = "2015-12-06T14:46:35+02:00"
tags = ["Gip","Nederlands"]
title = "Middagsessie Sway"
+++

Onze, [Ricardo](http://ricardo-coenen.github.io/GIPsite/) en Jens, middagsessie ging over Sway. Sway is de opvolger van PowerPoint, en is veel gebruiksvriendelijker. Als je niet veel van PowerPoint kent, is Sway de ideale oplossing voor u. Als je echter veel van PowerPoint kent en je wilt veel creativiteit gebruiken is PowerPoint nog steeds de koploper. Sway is goed, maar omdat het net uit preview is loopt het vaak vast en je kunt Sway ook niet gebruiken wanneer je geen internet hebt. Het is dus geen programma maar iets in je browser.

Hier onder vind je ook onze facebook pagina en onze FAQ:

[<img height="50px" src="https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcT9MOS_pH3Z-5vif6J49GnlMsJgQBsVgxbzYfcEZYiWwAd4Sr5M">](https://www.facebook.com/IT-tapa-588405757863108/)
[<img height="50px" src="http://icons.iconarchive.com/icons/icojam/blue-bits/256/question-faq-icon.png">](http://google.com.au/)

<img width="560" height="400"  src="https://scontent-ams2-1.xx.fbcdn.net/hphotos-xal1/t31.0-8/12240203_931268806910133_3764191703089532_o.jpg">

<img  width="560" height="400" src="https://scontent-ams2-1.xx.fbcdn.net/hphotos-xpf1/t31.0-8/12247988_931268696910144_4134057028211480645_o.jpg">
