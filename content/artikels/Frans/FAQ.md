+++
categories = ["Frans"]
date = "2015-12-05T14:46:35+02:00"
tags = ["IT-tapa"]
title = "FOIRE AUX QUESTIONS"
+++

**Vous avez des questions sur MS SWAY ? Voici les réponses !**

*Quel public est approprié pour sway ?*

    Sway est approprie pour tout public.

 *Est-ce qu’il est possible de mettre des graphiques et des tableaux?*

    Oui, c’est possible. Sway a des manières différentes pour ajouter des graphiques et des tableaux.

*Est-ce que Sway est en preview?*

    Non, Sway a maintenant une disponibilité générale.

*Quelle est la différence entre Sway et PowerPoint?*

    Sway est plus facile à faire des présentations que PowerPoint mais PowerPoint a été créé pour des personnes très creatives.

*Il y a des effets en Sway?*

    Oui, mais ce sont des effets prédéfinis.

*Est-ce qu’il est possible de créer un Sway sans Internet?*

    Non, Sway est une application en ligne.
